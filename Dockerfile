FROM iaminci/alpine:maven-3.9.6-openjdk-17 AS BUILD
WORKDIR /dummy
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src/ ./src/
RUN mvn package -DskipTests

FROM iaminci/alpine:openjdk-17-jre-headless
COPY --from=BUILD dummy/target/dummy-0.0.1.jar /dummy/dummy-0.0.1.jar
ENTRYPOINT ["java", "-jar", "dummy/dummy-0.0.1.jar"]